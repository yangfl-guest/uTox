#!/bin/sh
set -e

mkdir build
cd build
cmake .. -DENABLE_LTO=OFF -DENABLE_FILTERAUDIO=OFF -DENABLE_TESTS=ON -DSTBI_INCLUDE_DIR=/usr/include/stb
make test_chatlog
make test_chrono
make test
cd ..
rm -rf build
